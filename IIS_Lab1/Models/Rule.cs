﻿using System.Collections.Generic;
using System.Text;

namespace IIS_Lab1.Models
{
  public class Rule
  {
    public Rule(List<OneRule> prIf, OneRule prThen)
    {
      this.PrIf = prIf;
      this.PrThen = prThen;
    }

    public List<OneRule> PrIf { get; set; }
    public OneRule PrThen { get; set; }

    public override string ToString()
    {
      var builder = new StringBuilder();
      foreach (var oneRule in PrIf)
      {
        builder.Append(oneRule);
      }
      return "Rule [prIf=" + builder + ", prThen=" + PrThen + "]";
    }
  }
}