﻿namespace IIS_Lab1.Models
{
  public class OneRule
  {
    public OneRule(string name, string value)
    {
      this.Name = name;
      this.Value = value;
    }

    public string Name { get; set; }

    public string Value { get; set; }

    public override string ToString()
    {
      return "OneRule [name=" + Name + ", value=" + Value + "]";
    }
  }
}