﻿using System;
using System.Windows.Forms;

namespace IIS_Lab1
{
  public partial class AskForm : Form, IAskForm
  {
    private readonly Form _formOwner;

    private object _answer;

    public string Answer
    {
      get { return (string)_answer; }
    }

    public AskForm(Form formOwner)
    {
      InitializeComponent();
      _formOwner = formOwner;

    }

    public void ShowAskDialog(string title, object[] answers)
    {
      this.Text = title;
      comboBox1.Items.Clear();
      comboBox1.Items.AddRange(answers);
      if (answers.Length > 0)
      {
        comboBox1.SelectedIndex = 0;
      }
      this.ShowDialog(_formOwner);
    }

    private void button1_Click(object sender, EventArgs e)
    {
      _answer = comboBox1.SelectedItem;
      this.Hide();
    }
  }
}
