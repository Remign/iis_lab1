﻿using System;
using System.Windows.Forms;
using IIS_Lab1.Actions;

namespace IIS_Lab1
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new Form1(new ActionContext()));
    }
  }
}
