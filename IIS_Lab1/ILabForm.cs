﻿using System.Windows.Forms;

namespace IIS_Lab1
{
  public interface ILabForm
  {
    ComboBox ComboBox { get; }
    ListBox ListBox { get; }
  }
}