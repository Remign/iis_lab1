﻿namespace IIS_Lab1.Actions
{
  public interface IActionContext
  {
    string Execute(IAskForm askForm);
  }
}