﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using IIS_Lab1.Models;

namespace IIS_Lab1.Actions
{
  enum State { Lie = 1, Truth, Unknown };

  public class ActionContext : IActionContext, IActionInitializer
  {
    private List<Rule> Rules { get; set; }
    private ILabForm _labForm;
    private List<string> OutputList { get; set; }

    string IActionContext.Execute(IAskForm askForm)
    {
      OutputList.Clear();

      var logSign = false;
      var stackGoal = new List<string>();
      var stackGoalInt = new List<int>();
      var stackGoalBool = new List<bool>();
      var stackContext = new Stack<OneRule>();
      var activeRules = new List<Rule>(Rules);
      var discardRules = new List<Rule>();
      var acceptRules = new List<Rule>();

      var selectedString = (string)_labForm.ComboBox.SelectedItem;
      Debug.WriteLine(selectedString);
      stackGoal.Add(selectedString);
      stackGoalBool.Add(true);
      var anRule = -1;
      var analys = State.Unknown;

      while (!logSign)
      {
        Rule tempRule = null;
        bool find = false;
        Debug.WriteLine(stackGoal);
        if (anRule == -1)
        {
          foreach (var activeRule in activeRules)
          {
            if (activeRule.PrThen.Name.Equals(stackGoal[stackGoal.Count - 1])
              && !discardRules.Contains(activeRule)
              && !acceptRules.Contains(activeRule))
            {
              tempRule = activeRule;
              anRule = activeRules.IndexOf(activeRule);
              analys = State.Unknown;
              find = true;
              break;
            }
          }
        }
        if (anRule != -1)
        {
          var index = 0;
          foreach (var oneRule in activeRules[anRule].PrIf)
          {
            foreach (var stackContextRule in stackContext)
            {
              if (stackContextRule.Name.Equals(oneRule.Name))
              {
                if (!stackContextRule.Value.Equals(oneRule.Value))
                {
                  analys = State.Lie;
                }
                else
                {
                  index++;
                }
              }
            }
          }

          if (analys == State.Lie)
          {
          }
          else if (index == activeRules[anRule].PrIf.Count)
          {
            analys = State.Truth;
          }
          else
          {
            analys = State.Unknown;
          }
        }

        if (anRule != -1)
        {
          switch (analys)
          {
            case State.Lie:
              discardRules.Add(activeRules[anRule]);
              Debug.WriteLine("---Отбрасываемое правило : " + anRule);
              anRule = -1;
              break;
            case State.Truth:
              var acceptRule = activeRules[anRule].PrThen;
              acceptRules.Add(activeRules[anRule]);
              stackContext.Push(acceptRule);
              Debug.WriteLine("---Принимаемое правило : " + anRule + " " + acceptRule);
              OutputList.Add(" => " + acceptRule.Name + " = " + acceptRule.Value);
              string tempSdel = null;
              foreach (var item in stackGoal)
              {
                if (item.Equals(acceptRule.Name))
                {
                  tempSdel = item;
                  break;
                }
              }
              if (tempSdel != null)
              {
                var index = stackGoal.IndexOf(tempSdel);
                stackGoal.RemoveAt(index);
                stackGoalBool.RemoveAt(index);
              }
              if (stackGoal.Count == 0)
              {
                logSign = true;
              }
              anRule = -1;
              break;
            case State.Unknown:
              string next = null;
              foreach (var oneRule in activeRules[anRule].PrIf)
              {
                var tempBoolean = true;
                foreach (var stackContextRule in stackContext)
                {
                  if (oneRule.Name.Equals(stackContextRule.Name))
                  {
                    tempBoolean = false;
                  }
                }
                if (tempBoolean)
                {
                  next = oneRule.Name;
                  break;
                }
              }
              Debug.WriteLine("Стек целей добавлено 1 : " + next + " " + anRule);
              stackGoal.Add(next);
              stackGoalInt.Add(anRule);
              stackGoalBool.Add(true);
              anRule = -1;
              break;
          }
        }
        else if (stackGoal.Count > 1)
        {
          var s = stackGoal[stackGoal.Count - 1];
          stackGoal.RemoveAt(stackGoal.Count - 1);
          var b = stackGoalBool[stackGoalBool.Count - 1];
          stackGoalBool.RemoveAt(stackGoalBool.Count - 1);
          if (!b)
          {
            logSign = true;
          }
          else
          {
            HashSet<string> answers = new HashSet<string>();
            foreach (var activeRule in activeRules)
            {
              foreach (var oneRule in activeRule.PrIf)
              {
                if (oneRule.Name.Equals(s))
                {
                  answers.Add(oneRule.Value);
                }
              }
            }

            var list = answers.Select(item => item as object).ToArray();
            askForm.ShowAskDialog("select : " + s, list);
            string answer = askForm.Answer;
            Debug.WriteLine("Стек контекста добавлено : " + s + " " + answer);
            stackContext.Push(new OneRule(s, answer));
            OutputList.Add(s + " : " + answer);
            anRule = stackGoalInt.LastOrDefault();
            if (!find)
            {
              stackGoalBool.RemoveAt(stackGoalBool.Count - 1);
              stackGoalBool.Add(false);
            }
          }
        }
        else if (stackGoal.Count == 1 && !find)
        {
          logSign = true;
        }
      }
      var ans = "didn't find";
      if (stackContext.Count > 0)
      {
        var or = stackContext.Pop();
        if (or.Name.Equals(selectedString))
        {
          ans = or.Name + " : " + or.Value;
        }
      }

      _labForm.ListBox.Items.Clear();
      _labForm.ListBox.Items.AddRange(OutputList.Select(item => item as object).ToArray());

      return ans;
    }

    IActionContext IActionInitializer.InitializeActionContext(ILabForm form)
    {
      _labForm = form;
      Rules = new List<Rule>();
      OutputList = new List<string>();
      ReadFile();
      FillComboBox();

      return this;
    }

    private void ReadFile()
    {
      using (var reader = new StreamReader("test.txt"))
      {
        string line;
        while ((line = reader.ReadLine()) != null)
        {
          var words = line.Split(' ').Where(item => item != "=").ToArray();
          var enumerator = words.GetEnumerator();
          var oneRuleArr = new List<OneRule>();
          while (enumerator.MoveNext())
          {
            if ((string)enumerator.Current == "IF" || (string)enumerator.Current == "AND")
            {
              enumerator.MoveNext();
              var name = (string)enumerator.Current;
              enumerator.MoveNext();
              var value = (string)enumerator.Current;
              oneRuleArr.Add(new OneRule(name, value));
            }
            else if ((string)enumerator.Current == "THEN")
            {
              enumerator.MoveNext();
              var name = (string)enumerator.Current;
              enumerator.MoveNext();
              var value = (string)enumerator.Current;
              Rules.Add(new Rule(oneRuleArr, new OneRule(name, value)));
              break;
            }
          }
        }
      }
    }

    private void FillComboBox()
    {
      var goalArr = Rules.Select(item => item.PrThen.Name).GroupBy(item => item).Select(i => i.Key as object).ToArray();
      _labForm.ComboBox.Items.AddRange(goalArr);
      if (goalArr.Length > 0)
      {
        _labForm.ComboBox.SelectedIndex = 0;
      }
    }
  }
}