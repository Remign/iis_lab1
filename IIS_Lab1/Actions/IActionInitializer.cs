﻿namespace IIS_Lab1.Actions
{
  public interface IActionInitializer
  {
    IActionContext InitializeActionContext(ILabForm form);
  }
}