﻿using System;
using System.Windows.Forms;
using IIS_Lab1.Actions;

namespace IIS_Lab1
{
  public partial class Form1 : Form, ILabForm
  {
    private readonly IActionContext _actionContext;

    public ComboBox ComboBox
    {
      get { return this.comboBox1; }
    }

    public ListBox ListBox
    {
      get { return this.listBox1; }
    }

    public Form1(IActionInitializer actionInitializer)
    {
      InitializeComponent();
      _actionContext = actionInitializer.InitializeActionContext(this);
    }

    private void button1_Click(object sender, EventArgs e)
    {
      string answer;
      using (var askForm = new AskForm(this))
      {
        answer = _actionContext.Execute(askForm);
      }
      MessageBox.Show(this, answer);
    }
  }
}
