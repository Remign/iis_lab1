﻿namespace IIS_Lab1
{
  public interface IAskForm
  {
    void ShowAskDialog(string title, object[] answers);

    string Answer { get; }
  }
}